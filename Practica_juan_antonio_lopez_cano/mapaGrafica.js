d3.json('practica_airbnb.json')
        .then((featureCollection) => {
                dibujarMapaGrafica(featureCollection);
        });

function dibujarMapaGrafica(featureCollection) {


        console.log(featureCollection);
        console.log(featureCollection.features);

        var anchoMap = 800;        //width
        var altoMap = 1100;         //height

        //creo el lienzo del mapa
        var svgMapa = d3.select('div')
                .append('svg')
                .attr('width', anchoMap)
                .attr('height', altoMap)
                .append('g');

        //creo el centro del mapa
        var mapaCentro =
                d3.geoCentroid(featureCollection);
        console.log(`MAPA CENTRO ${mapaCentro}`);

        //creo la proyeccion
        var proyeccion = d3.geoMercator()
                .fitSize([anchoMap, altoMap], featureCollection);
        console.log(proyeccion);

        //crear path
        var proyeccionPath =
                d3.geoPath()
                        .projection(proyeccion);

        var scaleColor = [
                { key: 0, color: "#a2ccaf", rango: "15€ a 50€" },
                { key: 1, color: "#fcfecb", rango: "51€ a 100€" },
                { key: 2, color: "#bfe0e8", rango: "101€ a 150€" },
                { key: 3, color: "#fcd3e3", rango: "151€ a 280€" },
                { key: 4, color: "#1b262c", rango: "No existe el precio" }
        ];

        console.log(scaleColor);

        //creo el minimo y maximo del precio para la leyenda del mapa, en el que mostrare,
        //los precios por intervalos.
        var features = featureCollection.features;

        var precioMedioMin = d3.min(features, (d) => d.properties.avgprice);
        console.log(`PRECIO MEDIO MINIMO ${precioMedioMin} €`);
        var precioMedioMax = d3.max(features, (d) => d.properties.avgprice);
        console.log(`PRECIO MEDIO MAXIMO ${precioMedioMax} €`);

        //creo los path
        var crearPathMapa =
                svgMapa.append('g')
                        .selectAll('path')
                        .data(features)
                        .enter()
                        .append('path')
                        .attr('d', (d) => proyeccionPath(d))
                        .attr("fill", colorBarrios)
                        .attr('stroke', "black")
                        .attr('stroke-width', "0.5px")

                        //Eventos
                        .on('mouseover', verInfo)
                        .on('mouseout', ocultarInfo)
                        .on('click', clickMapa);

        var info =
                d3.select("div")
                        .append("div")
                        .style("position", "absolute")
                        .style("pointer-events", "none")
                        .style("visibility", "hidden")
                        .style("background-color", "white")
                        .style("border", "solid")
                        .style("border-width", "3px")
                        .style("border-radius", "10px")
                        .style("padding", "5px");


        //creo el lienzo de la grafica
        var anchoGraf = 1000;        //width
        var altoGraf = 1000;         //height

        var svgGrafica =
                d3.select('div')
                        .append('svg')
                        .attr('width', anchoGraf + 100)
                        .attr('height', altoGraf + 100)
                        .append("g")
                        .attr("transform", "translate(50,40)");



        //mapeo los datos para quedarme con los avgbedrooms
        var idBarrio = features.map(function (d) {
                return d.properties.avgbedrooms
        });

        //var filtroBarrio = idBarrio[10];
        //cada vez que recargue la pagina, muestra un barrio distinto.
        var randomBarrio = idBarrio[Math.floor(Math.random() * idBarrio.length)];

        //Creacion de escalas
        var xscale =
                d3.scaleBand()
                        .domain(randomBarrio.map(function (d) {
                                return d.bedrooms
                        }))
                        .range([0, anchoGraf])
                        .padding(0.3);


        var yscale =
                d3.scaleLinear()
                        .domain([0, d3.max(randomBarrio, function (d) {
                                return d.total;
                        })])
                        .range([altoGraf / 2, 0]);


        //Creación de eje X e Y
        var xaxis = d3.axisBottom(xscale);
        var yaxis = d3.axisLeft(yscale);

        //Añadimos el eje X
        svgGrafica.append("g")
                .attr("transform", "translate(0," + altoGraf / 2 + ")")
                .call(xaxis);

        //Añadimos el eje y
        svgGrafica.append("g")
                .attr("class", "yaxis")
                .call(yaxis);

        //Creacion de los rectangulos
        svgGrafica.append("g")
                .selectAll("rect")
                .data(randomBarrio)
                .enter()
                .append("rect")
                .attr("class", "rect")
                .attr("x", (d) => xscale(d.bedrooms))
                .attr("y", (d) => yscale(d.total))
                .attr("width", xscale.bandwidth())
                .attr("height", function (d) {
                        return altoGraf / 2 - yscale(d.total)
                })
                .attr("fill", "red");

        //Creacion del texto total por columna
        svgGrafica.append("g")
                .attr("class", "text")
                .selectAll("text")
                .data(randomBarrio)
                .enter()
                .append("text")
                .attr("x", (d) => xscale(d.bedrooms) + xscale.bandwidth() / 2)
                .attr("y", (d) => yscale(d.total))
                .text((d) => d.total);


        // Titulo  xaxis
        svgGrafica.append("text")
                .attr("transform", "translate(" + (anchoGraf / 2) + " ," + (altoGraf / 2 + 60) + ")")
                .style("text-anchor", "middle")
                .text("Número de Propiedades");

        // Titulo yaxis
        svgGrafica.append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 10)
                .attr("x", -100)
                .attr("dy", "1em")
                .style("text-anchor", "middle")
                .text("Número de Habitaciones");

        //funciones

        //color de los barrios
        function colorBarrios(d, i) {

                if (d.properties.avgprice >= precioMedioMin && d.properties.avgprice <= 50) {
                        return scaleColor[0].color;

                } else if (d.properties.avgprice > 50 && d.properties.avgprice <= 100) {
                        return scaleColor[1].color;

                } else if (d.properties.avgprice > 100 && d.properties.avgprice <= 150) {
                        return scaleColor[2].color;

                } else if (d.properties.avgprice > 150 && d.properties.avgprice <= precioMedioMax) {
                        return scaleColor[3].color;

                } else if (d.properties.avgprice == null) {
                        return scaleColor[4].color;
                }

        };

        //Captura de eventos Click

        function clickMapa(d, i) {
                barrioLeyendasEjeGrafica = d.properties.name;

                idBarrioClick = idBarrio[i + 1];

                console.log(`id: ${i}`);
                console.log(`idBarrioClick: ${idBarrioClick}`);
                yscale.domain([0, d3.max(idBarrioClick, function (d) {
                        return d.total;
                })]);


                //actualizo el eje y
                d3.selectAll(".yaxis")
                        .transition("borrar")
                        .duration(1000)
                        .call(yaxis);

                //elimino los rectangulos
                d3.selectAll(".rect").remove()
                        .transition()
                        .duration(1000);

                //elimino el texto de los rectangulos
                d3.selectAll(".text").remove()
                        .transition("borrar")
                        .duration(1000);

                //actualizo los rectangulos con los nuevos datos
                svgGrafica.append("g")
                        .selectAll("rect")
                        .data(idBarrioClick)
                        .enter()
                        .append("rect")
                        .attr("class", "rect")
                        .attr("x", (d) => xscale(d.bedrooms))
                        .attr("y", (d) => yscale(d.total))
                        .attr("width", xscale.bandwidth())
                        .attr("height", function (d) {
                                return altoGraf / 2 - yscale(d.total)
                        })
                        .attr("fill", "red");

                //Creacion del texto total por columna
                svgGrafica.append("g")
                        .selectAll("text")
                        .data(idBarrioClick)
                        .enter()
                        .append("text")
                        .attr("class", "text")
                        .attr("x", (d) => xscale(d.bedrooms) + xscale.bandwidth() / 2)
                        .attr("y", (d) => yscale(d.total))
                        .text((d) => d.total);



                console.log(`Barrio: ${d.properties.name}`);
                console.log(`Precio Medio: ${d.properties.avgprice}`);
                console.log(`ÌD Barrio: ${d.properties.cartodb_id}`);

        }

        // ver información de los barrios al pasar el raton 
        function verInfo(d, i) {
                d3.select(this)
                        .transition("transOver")
                        .duration(1000)
                        .attr("fill", "red")
                info.transition()
                        .duration(200)
                        .style("visibility", "visible")
                        .style("opacity", .9)
                        .style("left", (d3.event.pageX + 20) + "px")
                        .style("top", (d3.event.pageY - 30) + "px")
                        .text(`Barrio: ${d.properties.name}, Precio: ${d.properties.avgprice == null ? 'No tiene precio' : d.properties.avgprice} €`)
        }

        // ocultar información de los barrios al pasar el raton
        function ocultarInfo(d, i) {
                d3.select(this)
                        .transition("transOver")
                        .duration(200)
                        .attr("fill", colorBarrios)
                info.transition()
                        .duration(200)
                        .style("visibility", "hidden")


        }


        // crear leyenda de los colores del mapa
        var cantColoresArray = scaleColor.length;
        console.log(cantColoresArray);

        var anchoMapRectLey = (anchoMap / cantColoresArray) - 5;
        var altoMapRectLey = 20;

        var escalaLinearLey =
                d3.scaleLinear()
                        .domain([0, cantColoresArray])
                        .range([0, anchoMap]);


        var leyendaRect =
                svgMapa.append("g")
                        .selectAll("rect")
                        .data(scaleColor)
                        .enter()
                        .append("rect")
                        .attr("width", anchoMapRectLey)
                        .attr("height", altoMapRectLey)
                        .attr("x", (d, i) => escalaLinearLey(i))
                        .attr("fill", (d) => d.color);

        var leyendaRectText =
                svgMapa.append("g")
                        .selectAll("text")
                        .data(scaleColor)
                        .enter()
                        .append("text")
                        .attr("x", (d, i) => escalaLinearLey(i))
                        .attr("y", altoMapRectLey * 1.6)
                        .text((d) => d.rango)
                        .attr("font-size", 14);


}