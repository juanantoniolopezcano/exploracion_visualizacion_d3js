# Exploracion_Visualizacion_D3JS

Práctica del módulo de Exploración y visualización de datos D3 JS del BootCamp Full Stack Big Data AI y ML VI

## Explicación

Muestro, el mapa, de la ciudad de Madrid, por barrios, los colores varian segun segun categoria. La he dividido segun precios, al pasar el raton por cada barrio se cambia de color el barrio y muestra la información (nombre del barrio y precio medio).
A continuación muestro una gráfica en la que nos indica el número de habitaciones y el número de propiedades. Nada mas cargar la página, muestra un barrio aleatorio, en el momento que haces click en algun barrio del mapa la gráfica se actualiza con los valores de dicho barrio.